package mx.easypaid.marzo23

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import android.util.Log
import android.view.View
import android.widget.EditText
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {
    private var et1: EditText? = null

    //Método onCreate crea el activity de Login
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        //nnncd

        //Obtenemos el componente gráfico del layout. El id del boton es buttonEntrar
        val botonE = findViewById<Button>(R.id.buttonEntrar)

        //asignamos un listener para el evento de Onclick al boton
        botonE.setOnClickListener{
            // El siguiente código manda a llamar una actividad
            //TODO implementa el siguiente código en la acción de un botón
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

            //TODO compilar la libreía Volley para realizar el envio de peticiones HTTP

            /**val TAG = "EasyPaid"
            //Localhost
            val basePath = "http://192.168.100.51:5000/api/"
            val controlador = "Login"

            // Instantiate the RequestQueue
            val requestQueue = Volley.newRequestQueue(this.applicationContext);

            //Parámetros de envío para la petición de Login
            val params =   HashMap<String, String>()
            params.put("email", "alucard@gmail.com")
            params.put("password", "1231456")

            val parameters = JSONObject(params)

            //Prepare the Request
            val jsonObjReq = object : JsonObjectRequest(
                    Request.Method.POST, //GET
                    basePath + controlador, //URL
                    parameters, //Parameters
                    Response.Listener<JSONObject> { response ->
                        Log.d(TAG, "/GET request OK! Response: $response")
                        Toast.makeText(this.applicationContext,"GET request OK", Toast.LENGTH_LONG).show()

                        //val intent = Intent(this, MainActivity::class.java)
                        //startActivity(intent)
                    },
                    Response.ErrorListener { error ->
                        Log.d(TAG, "/GET request fail! Error: ${error.message}")
                        Toast.makeText(this.applicationContext,"Error en : ${error.message}", Toast.LENGTH_LONG).show()
                    }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers.put("Content-Type", "application/json")
                    return headers
                }
            }


            jsonObjReq.tag = TAG
            requestQueue?.add(jsonObjReq)*/

        }
    }

    //Método onStart se ejecuta e inicia el activity creado activity Login
    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "OnStart", Toast.LENGTH_SHORT).show()
        // La actividad está a punto de hacerse visible.
    }

    //Método onResume permite que el usuario pueda visualizar en pantalla el activity creado
    // activity Login se está ejecutando
    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "OnResume", Toast.LENGTH_SHORT).show()
        // La actividad se ha vuelto visible (ahora se "reanuda").
    }


    //Método onPause
    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "OnPause", Toast.LENGTH_SHORT).show()
        // Enfocarse en otra actividad  (esta actividad está a punto de ser "detenida").
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "OnStop", Toast.LENGTH_SHORT).show()
        // La actividad ya no es visible (ahora está "detenida")
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "OnDestroy", Toast.LENGTH_SHORT).show()
        // La actividad está a punto de ser destruida.
    }




}
